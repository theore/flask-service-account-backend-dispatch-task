# -*- coding: utf-8 -*-
import inspect
import logging
import os

import httplib2
from apiclient.discovery import build
from flask import redirect
from flask import request, jsonify, Blueprint
from flask import url_for
from google.appengine.api import memcache
from google.appengine.api import modules
from google.appengine.api import taskqueue
from oauth2client.contrib.appengine import AppAssertionCredentials
from oauth2client.service_account import ServiceAccountCredentials

from calendar_config import CALENDAR_ID
from settings import SERVICE_ACCOUNT_JSON

bp_backend_module = Blueprint('backend_module', __name__, url_prefix='/backend_module')


def backend_home(path=None):
    if os.environ.get('SERVER_SOFTWARE', 'Dev').startswith('Dev'):
        scopes = ['https://www.googleapis.com/auth/calendar']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            SERVICE_ACCOUNT_JSON, scopes)
    else:
        credentials = AppAssertionCredentials(scope='https://www.googleapis.com/auth/calendar')

    calendar_id = CALENDAR_ID
    http = credentials.authorize(httplib2.Http(memcache))
    service = build('calendar', 'v3')
    events = service.events().list(
        calendarId=calendar_id,
        singleEvents=True, maxResults=250,
        orderBy='startTime').execute(http=http)

    logging.info(events)
    return jsonify(events)
