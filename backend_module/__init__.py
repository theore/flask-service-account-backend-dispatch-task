# -*- coding: utf-8 -*-
import logging

from flask import current_app, Flask, redirect, url_for
from flask import render_template

import settings
from backend_module.views import backend_home


def create_app(config, debug=False, testing=False, config_overrides=None):
    app = Flask(__name__, template_folder=settings.TEMPLATE_FOLDER)
    app.config.from_object(config)

    app.debug = debug
    app.testing = testing

    if config_overrides:
        app.config.update(config_overrides)

    # Configure logging
    if not app.testing:
        logging.basicConfig(level=logging.INFO)

    # Register any blueprint.
    from backend_module.views import bp_backend_module
    app.register_blueprint(bp_backend_module)

    # [START app.route]
    @app.route('/_ah/start')
    def start():
        """App Engine warmup handler
        See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests

        """
        return 'OK'

    # [END app.route]
    # app.add_url_rule('/', 'home', view_func=backend_home, methods=['GET'])
    # app.add_url_rule('/<path:path>', 'home', view_func=backend_home, methods=['GET', 'POST'])
    app.add_url_rule('/my-backend/home', view_func=backend_home, methods=['GET', 'POST'])


    return app
