# -*- coding: utf-8 -*-
import inspect
import logging
import urllib2

from flask import render_template
from flask import request, jsonify, Blueprint
from google.appengine.api import modules
from google.appengine.api import taskqueue

bp_hello = Blueprint('hello', __name__, url_prefix='/hello')


@bp_hello.route('/', methods=['GET'])
def home():
    return render_template('index.html')


@bp_hello.route('/runtask', methods=['GET'])
def runtask():
    task = taskqueue.add(method='POST', queue_name='my-task', url='/my-backend/', params={'message': 'hello'})
    return 'Task {} enqueued, ETA {}.'.format(task.name, task.eta)
